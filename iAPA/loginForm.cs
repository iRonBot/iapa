﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using iAPA.Properties;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Web;
using System;
using iAPA.Classes;
using System.Text.RegularExpressions;
using iAPA.Resource;

namespace iAPA
{
  public partial class loginForm : Form
  {
    private static Uri URI;
    private static String LA;
    private static bool haveProxy;

    public loginForm() { InitializeComponent(); InitializeThread(); }

    private void InitializeThread()
    {
      loginThread.DoWork += new DoWorkEventHandler(TaskStart);
      loginThread.RunWorkerCompleted += new RunWorkerCompletedEventHandler(TaskCompleted);
    }
    private void TaskCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
      if(Equals(LA, Constants.FLAG.SUCCESSFUL))
      {
        this.Hide();
        mainForm mF = new mainForm(new Object[] { usernameBox.Text,
                                                passwordBox.Text,
                                                Regex.Match(serverBox.Text.ToLower(), Constants.REGEX.TRAVIAN).ToString().Replace(Constants.SLUG.DEFAULTSLUGS[9], Constants.SLUG.DEFAULTSLUGS[10]).ToLower(),
                                                Commons.SilverInventory,
                                                Commons.SID
                                              });
        mF.Show();
      }
      else
      {
        Utils.MessageDrawer(messageLabel, Constants.COLORS.ERROR, ResourceManager.Resource.GetString("WrongLoginData"));
      }
    }
    private void TaskStart(object sender, DoWorkEventArgs e) { if (base.InvokeRequired) { base.Invoke(new MethodInvoker(delegate { loginButton.Enabled = false; proxyButton.Enabled = false; })); } else { loginButton.Enabled = false; proxyButton.Enabled = false; } LA = (haveProxy) ? Commons.SilverCalculator(new Object[] { HttpUtility.UrlEncode(usernameBox.Text), HttpUtility.UrlEncode(passwordBox.Text), URI, true }) : Commons.SilverCalculator(new Object[] { HttpUtility.UrlEncode(usernameBox.Text), HttpUtility.UrlEncode(passwordBox.Text), URI, null }); }

    private void bitbucketLogo_MouseHover(object sender, EventArgs e) { bitbucketLogo.Image = Resources.BitBucket_On_24px; }
    private void bitbucketLogo_MouseLeave(object sender, EventArgs e) { bitbucketLogo.Image = Resources.BitBucket_Off_24px; }
    private void csharpLogo_MouseHover(object sender, EventArgs e) { csharpLogo.Image = Resources.C__On_24px; }
    private void csharpLogo_MouseLeave(object sender, EventArgs e) { csharpLogo.Image = Resources.C__Off_24px; }
    private void opensourceLogo_MouseHover(object sender, EventArgs e) { opensourceLogo.Image = Resources.OpenSource_On_24px; }
    private void opensourceLogo_MouseLeave(object sender, EventArgs e) { opensourceLogo.Image = Resources.OpenSource_Off_24px; }
    private void ccLogo_MouseHover(object sender, EventArgs e) { ccLogo.Image = Resources.CC_Logo_On_24px; }
    private void ccLogo_MouseLeave(object sender, EventArgs e) { ccLogo.Image = Resources.CC_Logo_Off_24px; }
    private void sunadobeLogo_MouseHover(object sender, EventArgs e) { sunadobeLogo.Image = Resources.SunAdobe_On_24px; }
    private void sunadobeLogo_MouseLeave(object sender, EventArgs e) { sunadobeLogo.Image = Resources.SunAdobe_Off_24px; }
    private void bitbucketLogo_Click(object sender, EventArgs e) { Process.Start("https://BitBucket.org/iRonBot/iapa"); }
    private void csharpLogo_Click(object sender, EventArgs e) { Process.Start("http://en.wikipedia.org/wiki/C_Sharp_%28programming_language%29"); }
    private void sunadobeLogo_Click(object sender, EventArgs e) { Process.Start("https://bitbucket.org/SunAdobe"); }
    private void opensourceLogo_Click(object sender, EventArgs e) { Process.Start("http://en.wikipedia.org/wiki/Open_source"); }
    private void ccLogo_Click(object sender, EventArgs e) { Process.Start("http://creativecommons.org/licenses/by-nc-sa/3.0/"); }
    private void proxyButton_Click(object sender, EventArgs e) { haveProxy = (Utils.pConfiguration()) ? haveProxy = true : haveProxy = false; }

    private void loginButton_Click(object sender, EventArgs e)
    {
      if (Utils.isEmpty(usernameBox.Text) ||
          Utils.isEmpty(passwordBox.Text) ||
          Utils.isEmpty(serverBox.Text))
      {
        /* تکمیل اطلاعات ضروری است */
        Utils.MessageDrawer(messageLabel, Constants.COLORS.ERROR, ResourceManager.Resource.GetString("FillDataRequired"));
      }
      else
      {
        if(!Regex.IsMatch(serverBox.Text, Constants.REGEX.TRAVIAN, RegexOptions.IgnoreCase))
        {
          /* آدرس سايت تراوين وارد شده منطبق نيست */
          Utils.MessageDrawer(messageLabel, Constants.COLORS.ERROR, ResourceManager.Resource.GetString("NotValidURL"));
        }
        else
        {
          URI = Utils.URL2URI(Regex.Match(serverBox.Text.ToLower(), Constants.REGEX.TRAVIAN).ToString());
          if (!NetworkInterface.GetIsNetworkAvailable())
          {
            /* شبکه در دسترس نیست */
            Utils.MessageDrawer(messageLabel, Constants.COLORS.ERROR, ResourceManager.Resource.GetString("NetworkLost"));
          }
          else
          {
            if(haveProxy)
            {
              if (!Utils.isValidURL(URI.ToString(), true))
              {
                /* آدرس وارد شده اشتباه است */
                Utils.MessageDrawer(messageLabel, Constants.COLORS.ERROR, ResourceManager.Resource.GetString("URLNotFound"));
              }
              else
              {
                /* در حال بارگذاری اطلاعات شما */
                Utils.MessageDrawer(messageLabel, Constants.COLORS.ATTENTION, ResourceManager.Resource.GetString("LoadingData"));
                loginThread.RunWorkerAsync();
              }
            }
            else
            {
              if (!Utils.isValidURL(URI.ToString()))
              {
                /* آدرس وارد شده اشتباه است */
                Utils.MessageDrawer(messageLabel, Constants.COLORS.ERROR, ResourceManager.Resource.GetString("URLNotFound"));
              }
              else
              {
                /* در حال بارگذاری اطلاعات شما */
                Utils.MessageDrawer(messageLabel, Constants.COLORS.ATTENTION, ResourceManager.Resource.GetString("LoadingData"));
                loginThread.RunWorkerAsync();
              }
            }
          }
        }
      }
    }
    /* &&&&&&&&&& */
  }
}