﻿using System.Net;
using System;
using System.Text;
using System.IO;
using iAPA.Resource;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using iAPA.Initialization;

namespace iAPA.Classes
{
  class Utils
  {
    public static CookieContainer SID = new CookieContainer();

    public static String RemoveSpecialCharacters(String Data)
    {
      return ((Data.Replace("\n", "")).Replace("\t", "")).Replace("\r", "");
    }
    public static String CheckItems(System.Windows.Forms.RadioButton[] DataInfo)
    {
      String IsChecked = Constants.FLAG.NONE;
      int i = 0;
      foreach(var CODE in Constants.ITEMS.CODES) { if(DataInfo[i].Checked){IsChecked = (CODE.Value).ToString();} i++; }
      return IsChecked;
    }
    public static System.Windows.Forms.RightToLeft Direction(String Current)
    {
      return ((Equals(Current.ToLower(), Constants.SLUG.DEFAULTSLUGS[8])) ? System.Windows.Forms.RightToLeft.Yes : System.Windows.Forms.RightToLeft.No);
    }
    public static void MessageDrawer(System.Windows.Forms.Label TARGET, System.Drawing.Color COLOR, String TEXT, bool isSplit = false, String dParam = null) { if (isSplit) { TARGET.ForeColor = COLOR; if (TARGET.InvokeRequired) { TARGET.Invoke(new MethodInvoker(delegate { TARGET.Text = String.Format(TEXT, dParam); })); } else { TARGET.Text = String.Format(TEXT, dParam); } } else { TARGET.ForeColor = COLOR; if (TARGET.InvokeRequired) { TARGET.Invoke(new MethodInvoker(delegate { TARGET.Text = TEXT; })); } else { TARGET.Text = TEXT; } } }
    public static bool isValidURL(String URL) { return isValidURL(URL, false); }
    public static bool isValidURL(String URL, bool hProxy = false)
    {
      try
      {
        WebProxy wProxy = new WebProxy();
        HttpWebRequest REQ = (HttpWebRequest)WebRequest.Create(String.Concat(URL, Constants.PAGES.LOGIN));
        if(hProxy)
        {
          wProxy = new WebProxy(Utils.URL2URI(String.Concat(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.NODE, Constants.PROXY.IP_NAME),
                                                                                                 Constants.SLUG.COLON,
                                                                                                 new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.NODE, Constants.PROXY.PORT_NAME))));
          wProxy.BypassProxyOnLocal = true;
        }
        REQ.Method = Constants.METHOD.POST;
        REQ.ContentLength = 0;
        REQ.KeepAlive = true;
        if (hProxy) { REQ.Proxy = wProxy; }
        HttpWebResponse RES = (HttpWebResponse)REQ.GetResponse();
        if (RES.Headers == null) { throw new Exception(Constants.ERRORS.RESULT_IS_NULL); }
        RES.Close();
        return true;
      }
      catch (WebException e)
      {
        e.ToString();
        return false;
      }
    }
    public static bool isEmpty(String Data)
    {
      if(String.IsNullOrEmpty(Data) ||
         String.IsNullOrWhiteSpace(Data) ||
         Equals(Data, ResourceManager.Resource.GetString("Username")) ||
         Equals(Data, ResourceManager.Resource.GetString("Password")) ||
         Equals(Data, ResourceManager.Resource.GetString("Server").ToLower()))
      {
        return true;
      }
      return false;
    }
    public static String pValue(String Data, String Begin, String End)
    {
      return pValue(Data, Begin, End, 1);
    }
    public static String pValue(String Data, String Begin, String End, int Pointer = 1)
    {
      try
      {
        int[] Counter = {0, 0, 0};

        for (int i = 0; i < Data.Length; i++)
        {
          if (Begin == Data.Substring(i, Begin.Length))
          {
            Counter[2] = i + Begin.Length;
            for (int j = Counter[2]; j < Data.Length; j++)
            {
              if (End == Data.Substring(j, End.Length))
              {
                Counter[0]++;
                Counter[1] = j;
                if (Counter[0] != Pointer) { break; }
                return Data.Substring(Counter[2], Counter[1] - Counter[2]);
              }
            }
          }
        }
        return Constants.ERRORS.CAN_NOT_FETCH;
      }
      catch (ArgumentException e)
      {
        e.ToString();
        return Constants.ERRORS.CAN_NOT_FETCH;
      }
    }
    public static String isExists(String Data, String Value) { return isExists(Data, Value, false, ""); }
    public static String isExists(String Data, String Value, bool Find, String Pointer)
    {
      String RET = Constants.ERRORS.CAN_NOT_FETCH;
      try
      {
        if(Find)
        {
          /* باید دنبال اطلاعات بگردم */
          RET = Data.Substring(Data.IndexOf(Value),
                               Data.Substring(Data.IndexOf(Value)).IndexOf(Pointer)).Replace(Value, "");
        }
        else
        {
          /* فقط باید بگم وجود دارد یا خیر */
          if (Data.IndexOf(Value) == -1)
          {
            RET = Constants.ERRORS.NOT_EXISTS;
          }
          else
          {
            RET = Constants.ERRORS.IS_EXISTS;
          }
        }
        return RET;
      }
      catch (ArgumentException e)
      {
        e.ToString();
        return RET;
      }
    }
    public static String _POST(String TARGETURL, String POSTDATA, String REFERER = "", bool USECOOKIE = false, CookieContainer COOKIE = null) { return _POSTLOGIN(TARGETURL, POSTDATA, REFERER, USECOOKIE, COOKIE, false); }
    public static String _POST(String TARGETURL, String POSTDATA, String REFERER = "", bool USECOOKIE = false, CookieContainer COOKIE = null, bool hProxy = false) { return _POSTLOGIN(TARGETURL, POSTDATA, REFERER, USECOOKIE, COOKIE, hProxy); }
    public static String _POSTLOGIN(String TARGETURL, String POSTDATA)
    {
      return _POSTLOGIN(TARGETURL, POSTDATA, "", false, null, false);
    }
    public static String _POSTLOGIN(String TARGETURL, String POSTDATA, bool hProxy = false)
    {
      return _POSTLOGIN(TARGETURL, POSTDATA, "", false, null, hProxy);
    }
    public static String _POSTLOGIN(String TARGETURL, String POSTDATA, String REFERER = "", bool USECOOKIE = false, CookieContainer COOKIE = null, bool hProxy = false)
    {
      try
      {
        WebProxy wProxy = new WebProxy();
        HttpWebRequest REQ = (HttpWebRequest)WebRequest.Create(TARGETURL);
        if (hProxy)
        {
          wProxy = new WebProxy(Utils.URL2URI(String.Concat(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.NODE, Constants.PROXY.IP_NAME),
                                                                                                 Constants.SLUG.COLON,
                                                                                                 new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.NODE, Constants.PROXY.PORT_NAME))));
          wProxy.BypassProxyOnLocal = true;
        }
        REQ.AllowAutoRedirect = true;
        REQ.UserAgent = Constants.USERAGENT.FIREFOX;
        REQ.KeepAlive = true;
        REQ.Method = Constants.METHOD.POST;
        REQ.Referer = REFERER;
        REQ.ServicePoint.Expect100Continue = false;
        REQ.ContentType = Constants.CONTENTTYPE.POST;
        REQ.ProtocolVersion = HttpVersion.Version10;
        REQ.CookieContainer = SID;
        // if (USECOOKIE) { REQ.CookieContainer = SID; }

        byte[] Bytes = Encoding.ASCII.GetBytes(POSTDATA);

        REQ.ContentLength = Bytes.Length;

        if (hProxy) { REQ.Proxy = wProxy; }

        Stream STM = REQ.GetRequestStream();

        STM.Write(Bytes, 0, Bytes.Length);

        STM.Close();

        HttpWebResponse RES = (HttpWebResponse)REQ.GetResponse();

        RES.Close();

        String VALUE = RES.Headers.ToString();
        return String.Concat(_gHTML(TARGETURL, TARGETURL),
                             Constants.UNIQUEPOINTER.B,
                             VALUE,
                             Constants.UNIQUEPOINTER.E);
      }
      catch (Exception e)
      {
        e.ToString();
        return Constants.ERRORS.CAN_NOT_LOGIN;
      }
    }
    public static String _gHTML(String URL, String REFERER, bool hProxy = false)
    {
      WebProxy wProxy = new WebProxy();
      HttpWebRequest REQ = (HttpWebRequest)WebRequest.Create(URL);
      if (hProxy)
      {
        wProxy = new WebProxy(Utils.URL2URI(String.Concat(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.NODE, Constants.PROXY.IP_NAME),
                                                                                               Constants.SLUG.COLON,
                                                                                               new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.NODE, Constants.PROXY.PORT_NAME))));
        wProxy.BypassProxyOnLocal = true;
      }
      REQ.Referer = REFERER;
      REQ.AllowAutoRedirect = true;
      REQ.ServicePoint.Expect100Continue = false;
      REQ.Timeout = 10000;
      REQ.CookieContainer = SID;
      REQ.Method = Constants.METHOD.GET;
      REQ.UserAgent = Constants.USERAGENT.FIREFOX;
      if (hProxy) { REQ.Proxy = wProxy; }
      HttpWebResponse RES = (HttpWebResponse)REQ.GetResponse();
      StreamReader SR = new StreamReader(RES.GetResponseStream());
      String rHTML = SR.ReadToEnd();
      SR.Close();
      RES.Close();
      return rHTML;
    }
    public static String _GETINFO(String URL) { return _GETINFO(URL, null, false, false); }
    public static String _GETINFO(String URL, bool hProxy = false) { return _GETINFO(URL, null, false, hProxy); }
    public static String _GETINFO(String URL, CookieContainer COOKIE, bool USECOOKIE) { return _GETINFO(URL, COOKIE, USECOOKIE, false); }
    public static String _GETINFO(String URL, CookieContainer COOKIE, bool USECOOKIE, bool hProxy = false)
    {
      ServicePointManager.Expect100Continue = false;
      try
      {
        WebProxy wProxy = new WebProxy();
        HttpWebRequest REQ = (HttpWebRequest)WebRequest.Create(URL);
        if (hProxy)
        {
          wProxy = new WebProxy(Utils.URL2URI(String.Concat(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.NODE, Constants.PROXY.IP_NAME),
                                                                                                 Constants.SLUG.COLON,
                                                                                                 new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.NODE, Constants.PROXY.PORT_NAME))));
          wProxy.BypassProxyOnLocal = true;
        }
        if (USECOOKIE) { REQ.CookieContainer = COOKIE; }
        REQ.UserAgent = Constants.USERAGENT.FIREFOX;
        REQ.KeepAlive = true;
        REQ.Method = Constants.METHOD.GET;
        if (hProxy) { REQ.Proxy = wProxy; }
        HttpWebResponse RES = (HttpWebResponse)REQ.GetResponse();
        if(USECOOKIE)
        {
          String KEY = new StreamReader(RES.GetResponseStream()).ReadToEnd();
          String VALUE = RES.Headers.ToString();
          return (KEY + Constants.UNIQUEPOINTER.B + VALUE + Constants.UNIQUEPOINTER.E);
        }
        else
        {
          StreamReader RED = new StreamReader(RES.GetResponseStream());
          return RED.ReadToEnd();
        }
      }
      catch
      {
        return Constants.ERRORS.EXCEPTION_OCCURRED;
      }
    }
    public static Uri URL2URI(String URL) { return URL2URI(URL, false); }
    public static Uri URL2URI(String URL, bool IsSplitted) { if (IsSplitted) { return new Uri(Constants.PROTOCOL.HTTP + URL.Replace(Regex.Match(URL, Constants.REGEX.SERVER).ToString(), Regex.Match(URL, Constants.REGEX.SERVER).ToString() + Constants.HTML.DEFAULTHTML[8].ToString())); } else { return new Uri(Constants.PROTOCOL.HTTP + URL); } }
    public static bool pConfiguration()
    {
      bool RES = false;

      String pInfo = Microsoft.VisualBasic.Interaction.InputBox(ResourceManager.Resource.GetString("ProxyText"),
                                                                ResourceManager.Resource.GetString("ProxyCaption"),
                                                                ResourceManager.Resource.GetString("ProxyDefault"));
      if (!Regex.IsMatch(pInfo, Constants.REGEX.PROXY, RegexOptions.IgnoreCase))
      {
        RES = false;
      }
      else
      {
        new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).WriteValue(Constants.PROXY.NODE, Constants.PROXY.IP_NAME, pInfo.Split(Constants.SLUG.COLON)[0].ToString());
        new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).WriteValue(Constants.PROXY.NODE, Constants.PROXY.PORT_NAME, pInfo.Split(Constants.SLUG.COLON)[1].ToString());
        RES = true;
      }
      return RES;
    }
  }
}
namespace iAPA.Resource
{
  public static class ResourceManager
  {
    private static System.Resources.ResourceManager CResource = new System.Resources.ResourceManager(iAPA.Classes.Constants.SLUG.DEFAULTSLUGS[7] + System.Globalization.CultureInfo.CurrentCulture.Name, System.Reflection.Assembly.GetExecutingAssembly());
    public static System.Resources.ResourceManager Resource {get { try{if(String.IsNullOrEmpty(CResource.GetString("ApplicationName"))){}}catch{CResource = new System.Resources.ResourceManager(iAPA.Classes.Constants.SLUG.DEFAULTSLUGS[7] + iAPA.Classes.Constants.SLUG.DEFAULTSLUGS[11], System.Reflection.Assembly.GetExecutingAssembly());} return CResource; } set { CResource = value; }}
  }
}
namespace iAPA.Initialization
{
  public class InitializationFile
  {
    private String cPath;
    [DllImport("kernel32")]
    private static extern long WritePrivateProfileString(String Section, String Key, String Val, String FilePath);
    [DllImport("kernel32")]
    private static extern int GetPrivateProfileString(String Section, String Key, String Def, StringBuilder RetVal, int Size, String FilePath);
    public InitializationFile(String iniPath) { cPath = iniPath; }
    public void WriteValue(String Section, String Key, String Value) { WritePrivateProfileString(Section, Key, Value, this.cPath); }
    public String ReadValue(String Section, String Key) { StringBuilder cData = new StringBuilder(255); int i = GetPrivateProfileString(Section, Key, "", cData, 255, this.cPath); return cData.ToString(); }
  }
}