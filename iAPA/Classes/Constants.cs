﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace iAPA.Classes
{
  class Constants
  {
    public class DEFAULTSILVER
    {
      public static int LOW = -1;
      public static int NORMAL = 100;
    }
    public class UNIQUEPOINTER
    {
      public static String B = "BEGIN";
      public static String E = "END";
    }
    public class CONTENTTYPE
    {
      public static String POST = "application/x-www-form-urlencoded";
    }
    public class USERAGENT
    {
      public static String FIREFOX = "Mozilla/5.0 (Windows NT 5.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2";
    }
    public class PROXY
    {
      public static String FILE_NAME = "IAPA_INITIALIZATION";
      public static String FILE_EXTENSION = ".ini";
      public static String NODE = "PROXY.CONFIGURE";
      public static String IP_NAME = "IP";
      public static String PORT_NAME = "PORT";
    }
    public class ITEMS
    {
      public static Dictionary<String, int> CODES = new Dictionary<String, int>()
      {
        {"OINTMENT", 11},
        {"EPIGRAPH", 10},
        {"BUCKET", 12},
        {"BRED", 14},
        {"BOOK", 13},
        {"ART", 15},
        {"GLUE", 7},
        {"BANDAGE", 8},
        {"CAGE", 9}
      };
    }
    public class REGEX
    {
      public static String TRAVIAN = "[a-z0-9]*.travian.[a-z.]*";
      public static String SERVER = "[a-z0-9]*.";
      public static String PROXY = "^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}:\\d{1,5}$";
    }
    public class COLORS
    {
      public static Color ERROR = Color.Red;
      public static Color ATTENTION = Color.Purple;
      public static Color SUCCESS = Color.Green;
      public static Color NONE = Color.Transparent;
    }
    public class SLUG
    {
      public static String SLASH = "/";
      public static char COLON = ':';
      public static String[] DEFAULTSLUGS = { /* 0 */ "name=",
                                              /* 1 */ "&password=",
                                              /* 2 */ "&login=1351257112",
                                              /* 3 */ "dorf1.php?ok=1",
                                              /* 4 */ "sess_id",
                                              /* 5 */ "lowRes",
                                              /* 6 */ "T3E",
                                              /* 7 */ "iAPA.Languages.",
                                              /* 8 */ "yes",
                                              /* 9 */ ".travian.",
                                              /* 10 */ ".",
                                              /* 11 */ "en-US",
                                              /*  */ "",
                                              /*  */ "",
                                              /*  */ "",
                                              /*  */ "",
                                              /*  */ "",
                                              /*  */ "",
                                              /*  */ "",
                                              /*  */ "",
                                              /*  */ "",
                                              /*  */ "",
                                              /*  */ "",
                                              "" };

    }
    public class METHOD
    {
      public static String GET = "GET";
      public static String POST = "POST";
    }
    public class PROTOCOL
    {
      public static String HTTP = "http://";
      public static String HTTPS = "https://";
    }
    public class PAGES
    {
      public static String LOGIN = "login.php";

    }
    public class FLAG
    {
      public static String NONE = "0";
      public static String WRONGUSERNAME = "1";
      public static String WRONGPASSWORD = "2";
      public static String SUCCESSFUL = "3";
      public static String FAIL = "4";
    }
    public class ERRORS
    {
      public static String CAN_NOT_FETCH = "errCanNotFetch";
      public static String NOT_EXISTS = "errNotExists";
      public static String IS_EXISTS = "errIsExists";
      public static String RESULT_IS_NULL = "errResultIsNull";
      public static String EXCEPTION_OCCURRED = "errExceptionOccurred";
      public static String CAN_NOT_LOGIN = "errCanNotLogin";
    }
    public class HTML
    {
      public static String[] DEFAULTHTML = { /* 0 */ "<div class=\"error",
                                             /* 1 */ "class=\"ajaxReplaceableSilverAmount\">",
                                             /* 2 */ "</span>",
                                             /* 3 */ "t-Cookie: sess_id=",
                                             /* 4 */ ",",
                                             /* 5 */ "T3E=",
                                             /* 6 */ ";",
                                             /* 7 */ "lowRes=",
                                             /* 8 */ "travian.",
                                             /*  */ "",
                                             /*  */ "",
                                             /*  */ "",
                                             /*  */ "",
                                             /*  */ "",
                                             /*  */ "",
                                             /*  */ "",
                                             /*  */ "",
                                             /*  */ "",
                                             /*  */ "",
                                             /*  */ "",
                                             "" };

    }
  }
}