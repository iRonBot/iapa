﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace iAPA.Classes
{
  class Commons
  {
    public static CookieContainer SID = new CookieContainer();
    public static String SilverInventory;


    public static String SilverCalculator(Object[] LoginInfo)
    {
      String SERVERANSWER = "";

      LoginInfo[0] = WebUtility.HtmlEncode((String) LoginInfo[0]);
      String POSTDATA = Constants.SLUG.DEFAULTSLUGS[0] +
                        LoginInfo[0].ToString() +
                        Constants.SLUG.DEFAULTSLUGS[1] +
                        LoginInfo[1].ToString() +
                        Constants.SLUG.DEFAULTSLUGS[2];
      if (LoginInfo[3] != null && (bool) LoginInfo[3]) { SERVERANSWER = Utils._POSTLOGIN(String.Concat(LoginInfo[2].ToString(), Constants.SLUG.DEFAULTSLUGS[3]), POSTDATA, true); } else { SERVERANSWER = Utils._POSTLOGIN(String.Concat(LoginInfo[2].ToString(), Constants.SLUG.DEFAULTSLUGS[3]), POSTDATA); }

      if(Equals(Utils.isExists(SERVERANSWER, Constants.HTML.DEFAULTHTML[0]), Constants.ERRORS.NOT_EXISTS))
      {
        SilverInventory = Utils.isExists(SERVERANSWER,
                                         Constants.HTML.DEFAULTHTML[1],
                                         true,
                                         Constants.HTML.DEFAULTHTML[2]);

        String[] COOKIEPROPERTY = { Utils.isExists(SERVERANSWER, Constants.HTML.DEFAULTHTML[3], true, Constants.HTML.DEFAULTHTML[4]),
                                    Utils.isExists(SERVERANSWER, Constants.HTML.DEFAULTHTML[5], true, Constants.HTML.DEFAULTHTML[6]),
                                    Utils.isExists(SERVERANSWER, Constants.HTML.DEFAULTHTML[7], true, Constants.HTML.DEFAULTHTML[6])};
        SID.Add(new Cookie(Constants.SLUG.DEFAULTSLUGS[4], COOKIEPROPERTY[0], Constants.SLUG.SLASH, Constants.HTML.DEFAULTHTML[8] + new Uri(LoginInfo[2].ToString()).Authority.ToString().Split('.')[2]));
        SID.Add(new Cookie(Constants.SLUG.DEFAULTSLUGS[5], COOKIEPROPERTY[2], Constants.SLUG.SLASH, Constants.HTML.DEFAULTHTML[8] + new Uri(LoginInfo[2].ToString()).Authority.ToString().Split('.')[2]));
        SID.Add(new Cookie(Constants.SLUG.DEFAULTSLUGS[6], COOKIEPROPERTY[1], Constants.SLUG.SLASH, Constants.HTML.DEFAULTHTML[8] + new Uri(LoginInfo[2].ToString()).Authority.ToString().Split('.')[2]));

        return Constants.FLAG.SUCCESSFUL;
      }
      else
      {
        return Constants.FLAG.WRONGPASSWORD;
      }
    }
  }
}