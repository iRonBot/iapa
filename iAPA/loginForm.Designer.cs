﻿using iAPA.Classes;
using iAPA.Resource;

namespace iAPA
{
  partial class loginForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;
    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }
    #region Windows Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(loginForm));
      this.loginLogo = new System.Windows.Forms.PictureBox();
      this.bitbucketLogo = new System.Windows.Forms.PictureBox();
      this.csharpLogo = new System.Windows.Forms.PictureBox();
      this.opensourceLogo = new System.Windows.Forms.PictureBox();
      this.ccLogo = new System.Windows.Forms.PictureBox();
      this.sunadobeLogo = new System.Windows.Forms.PictureBox();
      this.usernameBox = new System.Windows.Forms.TextBox();
      this.passwordBox = new System.Windows.Forms.MaskedTextBox();
      this.serverBox = new System.Windows.Forms.MaskedTextBox();
      this.loginButton = new System.Windows.Forms.Button();
      this.proxyButton = new System.Windows.Forms.Button();
      this.messageLabel = new System.Windows.Forms.Label();
      this.loginThread = new System.ComponentModel.BackgroundWorker();
      ((System.ComponentModel.ISupportInitialize)(this.loginLogo)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.bitbucketLogo)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.csharpLogo)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.opensourceLogo)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.ccLogo)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.sunadobeLogo)).BeginInit();
      this.SuspendLayout();
      // 
      // loginLogo
      // 
      this.loginLogo.Image = global::iAPA.Properties.Resources.Login_Logo;
      this.loginLogo.Location = new System.Drawing.Point(37, 7);
      this.loginLogo.Name = "loginLogo";
      this.loginLogo.Size = new System.Drawing.Size(222, 222);
      this.loginLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.loginLogo.TabStop = false;
      // 
      // bitbucketLogo
      // 
      this.bitbucketLogo.Cursor = System.Windows.Forms.Cursors.Hand;
      this.bitbucketLogo.Image = global::iAPA.Properties.Resources.BitBucket_Off_24px;
      this.bitbucketLogo.Location = new System.Drawing.Point(46, 317);
      this.bitbucketLogo.Name = "bitbucketLogo";
      this.bitbucketLogo.Size = new System.Drawing.Size(21, 24);
      this.bitbucketLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.bitbucketLogo.TabStop = false;
      this.bitbucketLogo.Click += new System.EventHandler(this.bitbucketLogo_Click);
      this.bitbucketLogo.MouseLeave += new System.EventHandler(this.bitbucketLogo_MouseLeave);
      this.bitbucketLogo.MouseHover += new System.EventHandler(this.bitbucketLogo_MouseHover);
      // 
      // csharpLogo
      // 
      this.csharpLogo.Cursor = System.Windows.Forms.Cursors.Hand;
      this.csharpLogo.Image = global::iAPA.Properties.Resources.C__Off_24px;
      this.csharpLogo.Location = new System.Drawing.Point(73, 317);
      this.csharpLogo.Name = "csharpLogo";
      this.csharpLogo.Size = new System.Drawing.Size(22, 24);
      this.csharpLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.csharpLogo.TabStop = false;
      this.csharpLogo.Click += new System.EventHandler(this.csharpLogo_Click);
      this.csharpLogo.MouseLeave += new System.EventHandler(this.csharpLogo_MouseLeave);
      this.csharpLogo.MouseHover += new System.EventHandler(this.csharpLogo_MouseHover);
      // 
      // opensourceLogo
      // 
      this.opensourceLogo.Cursor = System.Windows.Forms.Cursors.Hand;
      this.opensourceLogo.Image = global::iAPA.Properties.Resources.OpenSource_Off_24px;
      this.opensourceLogo.Location = new System.Drawing.Point(153, 317);
      this.opensourceLogo.Name = "opensourceLogo";
      this.opensourceLogo.Size = new System.Drawing.Size(24, 24);
      this.opensourceLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.opensourceLogo.TabStop = false;
      this.opensourceLogo.Click += new System.EventHandler(this.opensourceLogo_Click);
      this.opensourceLogo.MouseLeave += new System.EventHandler(this.opensourceLogo_MouseLeave);
      this.opensourceLogo.MouseHover += new System.EventHandler(this.opensourceLogo_MouseHover);
      // 
      // ccLogo
      // 
      this.ccLogo.Cursor = System.Windows.Forms.Cursors.Hand;
      this.ccLogo.Image = global::iAPA.Properties.Resources.CC_Logo_Off_24px;
      this.ccLogo.Location = new System.Drawing.Point(183, 317);
      this.ccLogo.Name = "ccLogo";
      this.ccLogo.Size = new System.Drawing.Size(68, 24);
      this.ccLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.ccLogo.TabStop = false;
      this.ccLogo.Click += new System.EventHandler(this.ccLogo_Click);
      this.ccLogo.MouseLeave += new System.EventHandler(this.ccLogo_MouseLeave);
      this.ccLogo.MouseHover += new System.EventHandler(this.ccLogo_MouseHover);
      // 
      // sunadobeLogo
      // 
      this.sunadobeLogo.Cursor = System.Windows.Forms.Cursors.Hand;
      this.sunadobeLogo.Image = global::iAPA.Properties.Resources.SunAdobe_Off_24px;
      this.sunadobeLogo.Location = new System.Drawing.Point(101, 317);
      this.sunadobeLogo.Name = "sunadobeLogo";
      this.sunadobeLogo.Size = new System.Drawing.Size(46, 24);
      this.sunadobeLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.sunadobeLogo.TabStop = false;
      this.sunadobeLogo.Click += new System.EventHandler(this.sunadobeLogo_Click);
      this.sunadobeLogo.MouseLeave += new System.EventHandler(this.sunadobeLogo_MouseLeave);
      this.sunadobeLogo.MouseHover += new System.EventHandler(this.sunadobeLogo_MouseHover);
      // 
      // usernameBox
      // 
      this.usernameBox.TabIndex = 0;
      this.usernameBox.ForeColor = System.Drawing.SystemColors.InactiveCaption;
      this.usernameBox.Location = new System.Drawing.Point(46, 235);
      this.usernameBox.Name = "usernameBox";
      this.usernameBox.Size = new System.Drawing.Size(100, 21);
      this.usernameBox.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      this.usernameBox.Text = ResourceManager.Resource.GetString("Username");
      // 
      // passwordBox
      // 
      this.passwordBox.TabIndex = 1;
      this.passwordBox.Culture = new System.Globalization.CultureInfo("");
      this.passwordBox.ForeColor = System.Drawing.SystemColors.InactiveCaption;
      this.passwordBox.Location = new System.Drawing.Point(151, 235);
      this.passwordBox.Name = "passwordBox";
      this.passwordBox.PasswordChar = '●';
      this.passwordBox.Size = new System.Drawing.Size(100, 21);
      this.passwordBox.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      this.passwordBox.Text = ResourceManager.Resource.GetString("Password");
      // 
      // serverBox
      // 
      this.serverBox.TabIndex = 2;
      this.serverBox.Culture = new System.Globalization.CultureInfo("");
      this.serverBox.ForeColor = System.Drawing.SystemColors.InactiveCaption;
      this.serverBox.Location = new System.Drawing.Point(46, 262);
      this.serverBox.Name = "serverBox";
      this.serverBox.Size = new System.Drawing.Size(125, 21);
      this.serverBox.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      this.serverBox.Text = ResourceManager.Resource.GetString("Server");
      // 
      // loginButton
      // 
      this.loginButton.TabIndex = 3;
      this.loginButton.Cursor = System.Windows.Forms.Cursors.Hand;
      this.loginButton.Location = new System.Drawing.Point(196, 262);
      this.loginButton.Name = "loginButton";
      this.loginButton.Size = new System.Drawing.Size(55, 21);
      this.loginButton.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      this.loginButton.Text = ResourceManager.Resource.GetString("Login");
      this.loginButton.UseVisualStyleBackColor = true;
      this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
      // 
      // proxyButton
      // 
      this.proxyButton.TabIndex = 4;
      this.proxyButton.Cursor = System.Windows.Forms.Cursors.Hand;
      this.proxyButton.Location = new System.Drawing.Point(174, 262);
      this.proxyButton.Name = "proxyButton";
      this.proxyButton.Size = new System.Drawing.Size(20, 21);
      this.proxyButton.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      this.proxyButton.Text = ResourceManager.Resource.GetString("ProxyButton");
      this.proxyButton.UseVisualStyleBackColor = true;
      this.proxyButton.Click += new System.EventHandler(this.proxyButton_Click);
      // 
      // messageLabel
      // 
      this.messageLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.messageLabel.Location = new System.Drawing.Point(34, 288);
      this.messageLabel.Name = "messageLabel";
      this.messageLabel.Size = new System.Drawing.Size(225, 23);
      this.messageLabel.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      this.messageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // loginThread
      // 
      this.loginThread.WorkerReportsProgress = true;
      this.loginThread.WorkerSupportsCancellation = true;
      // 
      // loginForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(294, 347);
      this.Controls.Add(this.messageLabel);
      this.Controls.Add(this.loginButton);
      this.Controls.Add(this.proxyButton);
      this.Controls.Add(this.serverBox);
      this.Controls.Add(this.passwordBox);
      this.Controls.Add(this.usernameBox);
      this.Controls.Add(this.sunadobeLogo);
      this.Controls.Add(this.ccLogo);
      this.Controls.Add(this.opensourceLogo);
      this.Controls.Add(this.csharpLogo);
      this.Controls.Add(this.bitbucketLogo);
      this.Controls.Add(this.loginLogo);
      this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.Name = "loginForm";
      this.RightToLeftLayout = true;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      this.Text = ResourceManager.Resource.GetString("ApplicationName");
      ((System.ComponentModel.ISupportInitialize)(this.loginLogo)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.bitbucketLogo)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.csharpLogo)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.opensourceLogo)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.ccLogo)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.sunadobeLogo)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();
    }
    #endregion
    private System.Windows.Forms.PictureBox loginLogo;
    private System.Windows.Forms.PictureBox bitbucketLogo;
    private System.Windows.Forms.PictureBox csharpLogo;
    private System.Windows.Forms.PictureBox opensourceLogo;
    private System.Windows.Forms.PictureBox ccLogo;
    private System.Windows.Forms.PictureBox sunadobeLogo;
    public  System.Windows.Forms.TextBox usernameBox;
    public  System.Windows.Forms.MaskedTextBox passwordBox;
    private System.Windows.Forms.MaskedTextBox serverBox;
    private System.Windows.Forms.Button loginButton;
    private System.Windows.Forms.Label messageLabel;
    private System.ComponentModel.BackgroundWorker loginThread;
    private System.Windows.Forms.Button proxyButton;
  }
}