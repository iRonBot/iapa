﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using iAPA.Classes;
using iAPA.Resource;
using System.Net;
using System.Threading;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace iAPA
{
  public partial class mainForm : Form
  {
    private static String ItemCode;
    private static Thread mainThread;
    private static bool haveProxy;

    public mainForm(Object[] LOGININFO) { InitializeComponent(); { KEYS = LOGININFO; usernameValue.Text = KEYS[0].ToString(); serverValue.Text = KEYS[2].ToString(); silverValue.Text = KEYS[3].ToString(); } }
    /* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ */
    private void proxyMenuItem_CheckedChanged(object sender, EventArgs e) { if (proxyMenuItem.Checked) { if (Utils.pConfiguration()) { proxyMenuItem.Checked = true; haveProxy = true; } else { proxyMenuItem.Checked = false; } } else { haveProxy = false; } }
    private void exitToolStripMenuItem_Click(object sender, EventArgs e) { Application.Exit(); }
    private void supportMenuItem_Click(object sender, EventArgs e) { Process.Start("http://chatserver.comm100.com/ChatWindow.aspx?siteId=33850&planId=659&visitType=1&byHref=1&partnerId=-1"); }
    private void homepageMenuItem_Click(object sender, EventArgs e) { Process.Start("http://ironbot.ir/"); }
    private void sourceMenuItem_Click(object sender, EventArgs e) { Process.Start("https://bitbucket.org/iRonBot/iapa"); }
    private void aboutMenuItem_Click(object sender, EventArgs e) { Process.Start("http://ironbot.ir/1391/10/19/iapa/"); }
    /* ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ */
    private void mainForm_FormClosed(object sender, FormClosedEventArgs e) { Application.Exit(); }
    private void startButton_Click(object sender, EventArgs e)
    {
      ItemCode = iAPA.Classes.Utils.CheckItems(new System.Windows.Forms.RadioButton[] { item1Button, item2Button, item3Button, item4Button, item5Button, item6Button, item7Button, item8Button, item9Button });

      if (Equals(ItemCode, Constants.FLAG.NONE))
      {
        /* یک جنس را برای شروع انتخاب کنید */
        Utils.MessageDrawer(messageLabel, Constants.COLORS.ERROR, ResourceManager.Resource.GetString("ChooseItemRequired"));
      }
      else
      {
        /* 100 */
        if(int.Parse(silverValue.Text) <= Constants.DEFAULTSILVER.NORMAL)
        {
          /* نقره‌ي شما براي شرکت در حراجي کم است */
          Utils.MessageDrawer(messageLabel, Constants.COLORS.ERROR, ResourceManager.Resource.GetString("NotEnoughSilver"));
        }
        else
        {
          String Result = (haveProxy) ? Utils._gHTML(String.Concat(new Object[] { Utils.URL2URI(serverValue.Text, true), "hero_auction.php?filter=", ItemCode, "&page=1" }), "", true) : Utils._gHTML(String.Concat(new Object[] { Utils.URL2URI(serverValue.Text, true), "hero_auction.php?filter=", ItemCode, "&page=1" }), "", false);

          /* ERROR Handling */
          String haveItem = Utils.isExists(Result, "<td class=\"noData\"");
          String zIndex = Utils.isExists(Result, "z=", true, "\"");
          if (Equals(Utils.RemoveSpecialCharacters(Utils.isExists(Result, "<div class=\"error\">", true, "</div>")), ResourceManager.Resource.GetString("LessTenAdventure")))
          {
            /* فقط بعد از 10 ماجراجویی می‌توانید در حراجی شرکت کنید */
            Utils.MessageDrawer(messageLabel, Constants.COLORS.ERROR, ResourceManager.Resource.GetString("AdventureMessage"));
          }
          else
          {
            if (Equals(haveItem, Constants.ERRORS.IS_EXISTS))
            {
              /* حراجی پیدا نشد */
              Utils.MessageDrawer(messageLabel, Constants.COLORS.ERROR, ResourceManager.Resource.GetString("NoAuctionsFound"));
            }
            else
            {
              if (base.InvokeRequired)
              {
                base.Invoke(new MethodInvoker(delegate
                {
                  startButton.Enabled = false;
                  stopButton.Enabled = true;
                  progressBar.Value = 0;
                  cogOffered.Text = progressBar.Value.ToString();
                  Utils.MessageDrawer(messageLabel, Constants.COLORS.NONE, "");
                }));
              }
              else
              {
                startButton.Enabled = false;
                stopButton.Enabled = true;
                progressBar.Value = 0;
                cogOffered.Text = progressBar.Value.ToString();
                Utils.MessageDrawer(messageLabel, Constants.COLORS.NONE, "");
              }
              mainThread = new Thread(new ThreadStart(doPurchase));
              mainThread.IsBackground = true;
              mainThread.Start();
            }
          }
        }
      }
    }
    private void stopButton_Click(object sender, EventArgs e)
    {
      if (base.InvokeRequired) { base.Invoke(new MethodInvoker(delegate { startButton.Enabled = true; stopButton.Enabled = false; })); } else { startButton.Enabled = true; stopButton.Enabled = false; }
      mainThread.Abort();
    }
    private void doPurchase()
    {
      MethodInvoker Invoke = null;
      int[] CrudeData = {(int) nopNumDown.Value, /* 0 - تعداد خرید | MAXNUM */
                         (int) mpoNumDown.Value, /* 1 - حداکثر قیمت خرید هر جنس | NUM */
                         (int) nopNumDown.Value, /* 2 - MAXP */
                         0,                      /* 3 - NUM2 */
                         0,                      /* 4 - NUM3 */
                         0};                     /* 5 - NUM7 */
      while(true)
      {
        if(int.Parse(silverValue.Text) <= Constants.DEFAULTSILVER.NORMAL)
        {
          /* نقره‌ي شما براي شرکت در حراجي کم است */
          Utils.MessageDrawer(messageLabel, Constants.COLORS.ERROR, ResourceManager.Resource.GetString("NotEnoughSilver"));
          break;
        }
        /* HERE */
        Dictionary<String, String> hValues = new Dictionary<String, String>();
        
        CrudeData[3]++;

        /* در حال بررسي اجناس صفحه‌، منتظر بمانيد */
        Utils.MessageDrawer(messageLabel, Constants.COLORS.ATTENTION, ResourceManager.Resource.GetString("CheckGenus"), true, CrudeData[3].ToString());

        String Result = (haveProxy) ? Utils._gHTML(String.Concat(new Object[] { Utils.URL2URI(serverValue.Text, true), "hero_auction.php?filter=", ItemCode, String.Concat("&page=", CrudeData[3].ToString()) }), "", true) : Utils._gHTML(String.Concat(new Object[] { Utils.URL2URI(serverValue.Text, true), "hero_auction.php?filter=", ItemCode, String.Concat("&page=", CrudeData[3].ToString()) }), "", false);

        /* ERROR Handling */
        String haveItem = Utils.isExists(Result, "<td class=\"noData\"");
        String zIndex = Utils.isExists(Result, "z=", true, "\"");

        if (Equals(Utils.RemoveSpecialCharacters(Utils.isExists(Result, "<div class=\"error\">", true, "</div>")), ResourceManager.Resource.GetString("LessTenAdventure")))
        {
          /* فقط بعد از 10 ماجراجویی می‌توانید در حراجی شرکت کنید */
          Utils.MessageDrawer(messageLabel, Constants.COLORS.ERROR, ResourceManager.Resource.GetString("AdventureMessage"));
        }
        else
        {
          if (Equals(haveItem, Constants.ERRORS.IS_EXISTS))
          {
            /* حراجی پیدا نشد */
            Utils.MessageDrawer(messageLabel, Constants.COLORS.ERROR, ResourceManager.Resource.GetString("NoAuctionsFound"));
          }
          else
          {
            /* شماره‌ی آخرین صفحه جنس انتخاب شده شناسایی می‌شود */
            String lastPage = Utils.isExists(Result, String.Concat("src=\"img/x.gif\" /></a> <a href=\"hero_auction.php?filter=", ItemCode, "&amp;page="), true, "\" class=\"last\">");

            CrudeData[4] = ((Equals(Utils.isExists(Result, "class=\"last disabled\"", true, "</div>"), Constants.ERRORS.CAN_NOT_FETCH))) ? int.Parse(lastPage) : 1;

            /* اطلاعات اقلام موجود در صف حراجی در متغییر ریخته شد */
            for (int i = 0; i < 20; i++)
            {
              String iParser = Utils.pValue(Result, "<td class=\"icon\">", "</a>", i + 1);

              if (!hValues.ContainsKey(Utils.isExists(iParser, "&amp;a=", true, "&amp")))
              {
                try { hValues.Add(Utils.pValue(iParser, String.Concat("&amp;filter=", ItemCode, "&amp;a="), "&amp;"), Utils.isExists(iParser, "<td  class=\"silver\" title=\"1						per unit\">", true, "</td>")); }
                catch { }
              }
            }
            /* حلقه‌ی ۲٠تایی برای سفارش دادن جنسهایی که با شرایط کاربر همخوان است */
            for (int i = 0; i < 20; i++)
            {
              try
              {
                if (((!(Equals(hValues.ElementAt(i).Value, Constants.ERRORS.CAN_NOT_FETCH))
                   &&
                   (int.Parse(hValues.ElementAt(i).Value) <= CrudeData[0])) && !(Equals(hValues.ElementAt(i).Key, Constants.ERRORS.CAN_NOT_FETCH)))
                   &&
                   !(Equals(hValues.ElementAt(i).Value, Constants.ERRORS.CAN_NOT_FETCH)))
                {
                  CrudeData[0] -= int.Parse(hValues.ElementAt(i).Value);
                  /* سرريز در اين بخش بايد اضافه شود */
                  CrudeData[5] = (CrudeData[1] * int.Parse(hValues.ElementAt(i).Value));

                  String doPurchase = (haveProxy) ? Utils._POST(Utils.URL2URI(String.Concat(serverValue.Text, Constants.SLUG.SLASH, "hero_auction.php"), true).ToString(), String.Concat("z=", zIndex, "&a=", hValues.ElementAt(i).Key, "&maxBid=", CrudeData[5].ToString()), "", true, (CookieContainer)KEYS[4], true) : Utils._POST(Utils.URL2URI(String.Concat(serverValue.Text, Constants.SLUG.SLASH, "hero_auction.php"), true).ToString(), String.Concat("z=", zIndex, "&a=", hValues.ElementAt(i).Key, "&maxBid=", CrudeData[5].ToString()), "", true, (CookieContainer)KEYS[4]);

                  Thread.Sleep(30);
                  String pAnswer = Utils.isExists(doPurchase, "<span class=\"c0 t\">", true, "</span>");

                  if (Equals(pAnswer, "0:00:0"))
                  {
                    Thread.Sleep(30);
                    zIndex = Utils.isExists(((haveProxy) ? Utils._gHTML(Utils.URL2URI(String.Concat(serverValue.Text, Constants.SLUG.SLASH, "hero_auction.php?filter=&page=", "100000000"), true).ToString(), "", true) : Utils._gHTML(Utils.URL2URI(String.Concat(serverValue.Text, Constants.SLUG.SLASH, "hero_auction.php?filter=&page=", "100000000"), true).ToString(), "", false)), "z=", true, "\"");
                  }
                  else
                  {
                    zIndex = Utils.isExists(pAnswer, "z=", true, "\"");
                  }
                  if (Invoke == null) { Invoke = delegate { progressBar.Maximum = CrudeData[2]; progressBar.Value = CrudeData[2] - CrudeData[0]; cogOffered.Text = progressBar.Value.ToString(); }; }
                  MethodInvoker beInvoke = Invoke;
                  if (base.InvokeRequired) { base.BeginInvoke(beInvoke); } else { beInvoke(); }
                  uSilver(doPurchase);
                }
              }
              catch { }
            }
            if (CrudeData[0] <= 0) { Utils.MessageDrawer(messageLabel, Constants.COLORS.SUCCESS, ResourceManager.Resource.GetString("PurchaseSuccessfully")); break; }
            if (CrudeData[3] >= CrudeData[4]) { Utils.MessageDrawer(messageLabel, Constants.COLORS.ERROR, ResourceManager.Resource.GetString("LowItems")); break; }
          }
        }
      }
      stopButton_Click(null, null);
    }
    private void uSilver(String cHTML)
    {
      MethodInvoker Invoke = null;
      int currentSilver = 0;

      if (!int.TryParse(Utils.pValue(cHTML, "class=\"silver\" /><br />", "</a>"), out currentSilver)) { }
      if (Invoke == null) { Invoke = delegate { silverValue.Text = currentSilver.ToString(); }; MethodInvoker beInvoke = Invoke; if (base.InvokeRequired) { base.BeginInvoke(beInvoke); } else { beInvoke(); } silverValue.Text = currentSilver.ToString(); }
    }
  }
}