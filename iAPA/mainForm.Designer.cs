﻿using iAPA.Classes;
using iAPA.Resource;
using System.Net;

namespace iAPA
{
  partial class mainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;
    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }
    #region Windows Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));

      this.headerLogo = new System.Windows.Forms.PictureBox();
      this.logininfoBox = new System.Windows.Forms.GroupBox();
      this.silverValue = new System.Windows.Forms.Label();
      this.serverValue = new System.Windows.Forms.Label();
      this.usernameValue = new System.Windows.Forms.Label();
      this.silverLabel = new System.Windows.Forms.Label();
      this.serverLabel = new System.Windows.Forms.Label();
      this.usernameLabel = new System.Windows.Forms.Label();
      this.taskpanelBox = new System.Windows.Forms.GroupBox();
      this.startButton = new System.Windows.Forms.Button();
      this.stopButton = new System.Windows.Forms.Button();
      this.cogOffered = new System.Windows.Forms.Label();
      this.cogLabel = new System.Windows.Forms.Label();
      this.mpoNumDown = new System.Windows.Forms.NumericUpDown();
      this.mpoLabel = new System.Windows.Forms.Label();
      this.nopNumDown = new System.Windows.Forms.NumericUpDown();
      this.nopLabel = new System.Windows.Forms.Label();
      this.item1Button = new System.Windows.Forms.RadioButton();
      this.item2Button = new System.Windows.Forms.RadioButton();
      this.item3Button = new System.Windows.Forms.RadioButton();
      this.item4Button = new System.Windows.Forms.RadioButton();
      this.item5Button = new System.Windows.Forms.RadioButton();
      this.item6Button = new System.Windows.Forms.RadioButton();
      this.item7Button = new System.Windows.Forms.RadioButton();
      this.item8Button = new System.Windows.Forms.RadioButton();
      this.item9Button = new System.Windows.Forms.RadioButton();
      this.progressBar = new System.Windows.Forms.ProgressBar();
      this.messageLabel = new System.Windows.Forms.Label();
      this.mainMenu = new System.Windows.Forms.MenuStrip();
      this.fileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.helpMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.aboutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.sourceMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.supportMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.homepageMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.proxyMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.fSeparator = new System.Windows.Forms.ToolStripSeparator();
      this.sSeparator = new System.Windows.Forms.ToolStripSeparator();

      ((System.ComponentModel.ISupportInitialize)(this.headerLogo)).BeginInit();
      this.logininfoBox.SuspendLayout();
      this.taskpanelBox.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.mpoNumDown)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nopNumDown)).BeginInit();
      this.SuspendLayout();
      // 
      // headerLogo
      // 
      this.headerLogo.Image = (Equals(ResourceManager.Resource.GetString("RightToLeft").ToLower(), Constants.SLUG.DEFAULTSLUGS[8])) ? global::iAPA.Properties.Resources.Header_Background : global::iAPA.Properties.Resources.Header_Background;
      this.headerLogo.Location = new System.Drawing.Point(0, 24);
      this.headerLogo.Name = "headerLogo";
      this.headerLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      // 
      // logininfoBox
      // 
      this.logininfoBox.Controls.Add(this.silverValue);
      this.logininfoBox.Controls.Add(this.serverValue);
      this.logininfoBox.Controls.Add(this.usernameValue);
      this.logininfoBox.Controls.Add(this.silverLabel);
      this.logininfoBox.Controls.Add(this.serverLabel);
      this.logininfoBox.Controls.Add(this.usernameLabel);
      this.logininfoBox.Location = new System.Drawing.Point(3, 136);
      this.logininfoBox.Name = "logininfoBox";
      this.logininfoBox.Size = new System.Drawing.Size(200, 100);
      this.logininfoBox.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      this.logininfoBox.Text = ResourceManager.Resource.GetString("AccountInfo");
      // 
      // silverValue
      // 
      this.silverValue.Location = (Equals(ResourceManager.Resource.GetString("RightToLeft").ToLower(), Constants.SLUG.DEFAULTSLUGS[8])) ? new System.Drawing.Point(7, 74) : new System.Drawing.Point(94, 74);
      this.silverValue.Name = "silverValue";
      this.silverValue.AutoSize = true;
      this.silverValue.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      // 
      // serverValue
      // 
      this.serverValue.Location = (Equals(ResourceManager.Resource.GetString("RightToLeft").ToLower(), Constants.SLUG.DEFAULTSLUGS[8])) ? new System.Drawing.Point(7, 49) : new System.Drawing.Point(49, 49);
      this.serverValue.Name = "serverValue";
      this.serverValue.AutoSize = true;
      this.serverValue.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      // 
      // usernameValue
      // 
      this.usernameValue.Location = (Equals(ResourceManager.Resource.GetString("RightToLeft").ToLower(), Constants.SLUG.DEFAULTSLUGS[8])) ? new System.Drawing.Point(7, 23) : new System.Drawing.Point(65, 23);
      this.usernameValue.Name = "usernameValue";
      this.usernameValue.AutoSize = true;
      this.usernameValue.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      // 
      // silverLabel
      // 
      this.silverLabel.Location = (Equals(ResourceManager.Resource.GetString("RightToLeft").ToLower(), Constants.SLUG.DEFAULTSLUGS[8])) ? new System.Drawing.Point(123, 74) : new System.Drawing.Point(7, 74);
      this.silverLabel.Name = "silverLabel";
      this.silverLabel.AutoSize = true;
      this.silverLabel.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      this.silverLabel.Text = ResourceManager.Resource.GetString("SilverInventory");
      // 
      // serverLabel
      // 
      this.serverLabel.Location = (Equals(ResourceManager.Resource.GetString("RightToLeft").ToLower(), Constants.SLUG.DEFAULTSLUGS[8])) ? new System.Drawing.Point(137, 49) : new System.Drawing.Point(7, 49);
      this.serverLabel.Name = "serverLabel";
      this.serverLabel.AutoSize = true;
      this.serverLabel.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      this.serverLabel.Text = ResourceManager.Resource.GetString("ServerValue");
      // 
      // usernameLabel
      // 
      this.usernameLabel.Location = (Equals(ResourceManager.Resource.GetString("RightToLeft").ToLower(), Constants.SLUG.DEFAULTSLUGS[8])) ? new System.Drawing.Point(147, 24) : new System.Drawing.Point(7, 24);
      this.usernameLabel.Name = "usernameLabel";
      this.usernameLabel.AutoSize = true;
      this.usernameLabel.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      this.usernameLabel.Text = ResourceManager.Resource.GetString("UsernameValue");
      // 
      // taskpanelBox
      // 
      this.taskpanelBox.Controls.Add(this.startButton);
      this.taskpanelBox.Controls.Add(this.stopButton);
      this.taskpanelBox.Controls.Add(this.cogLabel);
      this.taskpanelBox.Controls.Add(this.mpoNumDown);
      this.taskpanelBox.Controls.Add(this.mpoLabel);
      this.taskpanelBox.Controls.Add(this.nopNumDown);
      this.taskpanelBox.Controls.Add(this.nopLabel);
      this.taskpanelBox.Location = new System.Drawing.Point(206, 136);
      this.taskpanelBox.Name = "taskpanelBox";
      this.taskpanelBox.Size = new System.Drawing.Size(448, 100);
      this.taskpanelBox.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      this.taskpanelBox.Text = ResourceManager.Resource.GetString("TaskPanel");
      // 
      // startButton
      // 
      this.startButton.TabIndex = 2;
      this.startButton.BackgroundImage = global::iAPA.Properties.Resources.Play;
      this.startButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      this.startButton.Location = (Equals(ResourceManager.Resource.GetString("RightToLeft").ToLower(), Constants.SLUG.DEFAULTSLUGS[8])) ? new System.Drawing.Point(7, 54) : new System.Drawing.Point(354, 54);
      this.startButton.Name = "startButton";
      this.startButton.Size = new System.Drawing.Size(40, 40);
      this.startButton.Click += new System.EventHandler(this.startButton_Click);
      // 
      // stopButton
      // 
      this.stopButton.TabIndex = 3;
      this.stopButton.BackgroundImage = global::iAPA.Properties.Resources.Stop;
      this.stopButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      this.stopButton.Location = (Equals(ResourceManager.Resource.GetString("RightToLeft").ToLower(), Constants.SLUG.DEFAULTSLUGS[8])) ? new System.Drawing.Point(53, 54) : new System.Drawing.Point(400, 54);
      this.stopButton.Name = "stopButton";
      this.stopButton.Size = new System.Drawing.Size(40, 40);
      this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
      this.stopButton.Enabled = false;
      // 
      // cogOffered
      // 
      this.cogOffered.Location = (Equals(ResourceManager.Resource.GetString("RightToLeft").ToLower(), Constants.SLUG.DEFAULTSLUGS[8])) ? new System.Drawing.Point(355, 210) : new System.Drawing.Point(340, 210);
      this.cogOffered.ForeColor = Constants.COLORS.ATTENTION;
      this.cogOffered.Name = "cogOffered";
      this.cogOffered.AutoSize = true;
      this.cogOffered.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      this.cogOffered.Text = ResourceManager.Resource.GetString("OfferedValue");
      // 
      // cogLabel
      // 
      this.cogLabel.Location = (Equals(ResourceManager.Resource.GetString("RightToLeft").ToLower(), Constants.SLUG.DEFAULTSLUGS[8])) ? new System.Drawing.Point(296, 74) : new System.Drawing.Point(7, 74);
      this.cogLabel.Name = "cogLabel";
      this.cogLabel.AutoSize = true;
      this.cogLabel.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      this.cogLabel.Text = ResourceManager.Resource.GetString("COG");
      // 
      // mpoNumDown
      // 
      this.mpoNumDown.TabIndex = 1;
      this.mpoNumDown.Location = (Equals(ResourceManager.Resource.GetString("RightToLeft").ToLower(), Constants.SLUG.DEFAULTSLUGS[8])) ? new System.Drawing.Point(215, 21) : new System.Drawing.Point(372, 21);
      this.mpoNumDown.Maximum = new decimal(new int[] {999999,0,0,0});
      this.mpoNumDown.Minimum = new decimal(new int[] {1,0,0,0});
      this.mpoNumDown.Name = "mpoNumDown";
      this.mpoNumDown.AutoSize = true;
      this.mpoNumDown.ReadOnly = true;
      this.mpoNumDown.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      this.mpoNumDown.Size = new System.Drawing.Size(68, 21);
      this.mpoNumDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.mpoNumDown.Value = new decimal(new int[] {1,0,0,0});
      // 
      // mpoLabel
      // 
      this.mpoLabel.Location = (Equals(ResourceManager.Resource.GetString("RightToLeft").ToLower(), Constants.SLUG.DEFAULTSLUGS[8])) ? new System.Drawing.Point(283, 24) : new System.Drawing.Point(166, 24);
      this.mpoLabel.Name = "mpoLabel";
      this.mpoLabel.AutoSize = true;
      this.mpoLabel.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      this.mpoLabel.Text = ResourceManager.Resource.GetString("MPO");
      // 
      // nopNumDown
      // 
      this.nopNumDown.TabIndex = 0;
      this.nopNumDown.Location = (Equals(ResourceManager.Resource.GetString("RightToLeft").ToLower(), Constants.SLUG.DEFAULTSLUGS[8])) ? new System.Drawing.Point(7, 21) : new System.Drawing.Point(120, 21);
      this.nopNumDown.Maximum = new decimal(new int[] {999,0,0,0});
      this.nopNumDown.Minimum = new decimal(new int[] {1,0,0,0});
      this.nopNumDown.Name = "nopNumDown";
      this.nopNumDown.AutoSize = true;
      this.nopNumDown.ReadOnly = true;
      this.nopNumDown.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      this.nopNumDown.Size = new System.Drawing.Size(40, 21);
      this.nopNumDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nopNumDown.Value = new decimal(new int[] {1,0,0,0});
      // 
      // nopLabel
      // 
      this.nopLabel.Location = (Equals(ResourceManager.Resource.GetString("RightToLeft").ToLower(), Constants.SLUG.DEFAULTSLUGS[8])) ? new System.Drawing.Point(48, 24) : new System.Drawing.Point(7, 24);
      this.nopLabel.Name = "nopLabel";
      this.nopLabel.AutoSize = true;
      this.nopLabel.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      this.nopLabel.Text = ResourceManager.Resource.GetString("NOP");
      // 
      // item1Button
      // 
      this.item1Button.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.item1Button.Appearance = System.Windows.Forms.Appearance.Button;
      this.item1Button.Image = ((System.Drawing.Image)(resources.GetObject("item1Button.Image")));
      this.item1Button.Location = new System.Drawing.Point(6, 244);
      this.item1Button.Name = "item1Button";
      this.item1Button.Size = new System.Drawing.Size(69, 70);
      this.item1Button.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.item1Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
      // 
      // item2Button
      // 
      this.item2Button.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.item2Button.Appearance = System.Windows.Forms.Appearance.Button;
      this.item2Button.Image = ((System.Drawing.Image)(resources.GetObject("item2Button.Image")));
      this.item2Button.Location = new System.Drawing.Point(78, 244);
      this.item2Button.Name = "item2Button";
      this.item2Button.Size = new System.Drawing.Size(69, 70);
      this.item2Button.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.item2Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
      // 
      // item3Button
      // 
      this.item3Button.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.item3Button.Appearance = System.Windows.Forms.Appearance.Button;
      this.item3Button.Image = ((System.Drawing.Image)(resources.GetObject("item3Button.Image")));
      this.item3Button.Location = new System.Drawing.Point(150, 244);
      this.item3Button.Name = "item3Button";
      this.item3Button.Size = new System.Drawing.Size(69, 70);
      this.item3Button.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.item3Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
      // 
      // item4Button
      // 
      this.item4Button.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.item4Button.Appearance = System.Windows.Forms.Appearance.Button;
      this.item4Button.Image = ((System.Drawing.Image)(resources.GetObject("item4Button.Image")));
      this.item4Button.Location = new System.Drawing.Point(222, 244);
      this.item4Button.Name = "item4Button";
      this.item4Button.Size = new System.Drawing.Size(69, 70);
      this.item4Button.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.item4Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
      // 
      // item5Button
      // 
      this.item5Button.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.item5Button.Appearance = System.Windows.Forms.Appearance.Button;
      this.item5Button.Image = ((System.Drawing.Image)(resources.GetObject("item5Button.Image")));
      this.item5Button.Location = new System.Drawing.Point(294, 244);
      this.item5Button.Name = "item5Button";
      this.item5Button.Size = new System.Drawing.Size(69, 70);
      this.item5Button.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.item5Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
      // 
      // item6Button
      // 
      this.item6Button.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.item6Button.Appearance = System.Windows.Forms.Appearance.Button;
      this.item6Button.Image = ((System.Drawing.Image)(resources.GetObject("item6Button.Image")));
      this.item6Button.Location = new System.Drawing.Point(366, 244);
      this.item6Button.Name = "item6Button";
      this.item6Button.Size = new System.Drawing.Size(69, 70);
      this.item6Button.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.item6Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
      // 
      // item7Button
      // 
      this.item7Button.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.item7Button.Appearance = System.Windows.Forms.Appearance.Button;
      this.item7Button.Image = global::iAPA.Properties.Resources.Item_07;
      this.item7Button.Location = new System.Drawing.Point(438, 244);
      this.item7Button.Name = "item7Button";
      this.item7Button.Size = new System.Drawing.Size(69, 70);
      this.item7Button.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.item7Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
      // 
      // item8Button
      // 
      this.item8Button.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.item8Button.Appearance = System.Windows.Forms.Appearance.Button;
      this.item8Button.Image = global::iAPA.Properties.Resources.Item_08;
      this.item8Button.Location = new System.Drawing.Point(510, 244);
      this.item8Button.Name = "item8Button";
      this.item8Button.Size = new System.Drawing.Size(69, 70);
      this.item8Button.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.item8Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
      // 
      // item9Button
      // 
      this.item9Button.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.item9Button.Appearance = System.Windows.Forms.Appearance.Button;
      this.item9Button.Image = global::iAPA.Properties.Resources.Item_09;
      this.item9Button.Location = new System.Drawing.Point(582, 244);
      this.item9Button.Name = "item9Button";
      this.item9Button.Size = new System.Drawing.Size(69, 70);
      this.item9Button.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.item9Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
      // 
      // progressBar
      // 
      this.progressBar.Anchor = System.Windows.Forms.AnchorStyles.None;
      this.progressBar.Location = new System.Drawing.Point(3, 322);
      this.progressBar.Name = "progressBar";
      this.progressBar.Size = new System.Drawing.Size(651, 25);
      this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
      this.progressBar.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      // 
      // mainMenu
      // 
      this.mainMenu.AllowMerge = false;
      this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenuItem,
            this.toolsMenuItem,
            this.helpMenuItem});
      this.mainMenu.Location = new System.Drawing.Point(0, 0);
      this.mainMenu.Name = "mainMenu";
      this.mainMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
      this.mainMenu.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      this.mainMenu.Size = new System.Drawing.Size(292, 24);
      this.mainMenu.TabIndex = 0;
      this.mainMenu.Text = "mainMenu";
      // 
      // fileMenuItem
      // 
      this.fileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
      this.fileMenuItem.Name = "fileMenuItem";
      this.fileMenuItem.Size = new System.Drawing.Size(35, 20);
      this.fileMenuItem.Text = ResourceManager.Resource.GetString("fileMenuItem");
      // 
      // exitToolStripMenuItem
      // 
      this.exitToolStripMenuItem.Name = "exitMenuItem";
      this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
      this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
      this.exitToolStripMenuItem.Text = ResourceManager.Resource.GetString("exitMenuItem");
      this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
      // 
      // helpMenuItem
      // 
      this.helpMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.supportMenuItem,
            this.homepageMenuItem,
            this.fSeparator,
            this.sourceMenuItem,
            this.sSeparator,
            this.aboutMenuItem});
      this.helpMenuItem.Name = "helpMenuItem";
      this.helpMenuItem.Size = new System.Drawing.Size(40, 20);
      this.helpMenuItem.Text = ResourceManager.Resource.GetString("helpMenuItem");
      // 
      // aboutMenuItem
      // 
      this.aboutMenuItem.Name = "aboutMenuItem";
      this.aboutMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
      this.aboutMenuItem.Size = new System.Drawing.Size(159, 22);
      this.aboutMenuItem.Text = ResourceManager.Resource.GetString("aboutMenuItem");
      this.aboutMenuItem.Click += new System.EventHandler(this.aboutMenuItem_Click);
      // 
      // sourceMenuItem
      // 
      this.sourceMenuItem.Name = "sourceMenuItem";
      this.sourceMenuItem.Size = new System.Drawing.Size(159, 22);
      this.sourceMenuItem.Text = ResourceManager.Resource.GetString("sourceMenuItem");
      this.sourceMenuItem.Click += new System.EventHandler(this.sourceMenuItem_Click);
      // 
      // supportMenuItem
      // 
      this.supportMenuItem.Name = "supportMenuItem";
      this.supportMenuItem.Size = new System.Drawing.Size(159, 22);
      this.supportMenuItem.Text = ResourceManager.Resource.GetString("supportMenuItem");
      this.supportMenuItem.Click += new System.EventHandler(this.supportMenuItem_Click);
      // 
      // reportMenuItem
      // 
      this.homepageMenuItem.Name = "homepageMenuItem";
      this.homepageMenuItem.Size = new System.Drawing.Size(159, 22);
      this.homepageMenuItem.Text = ResourceManager.Resource.GetString("homepageMenuItem");
      this.homepageMenuItem.Click += new System.EventHandler(this.homepageMenuItem_Click);
      // 
      // toolsMenuItem
      // 
      this.toolsMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.proxyMenuItem});
      this.toolsMenuItem.Name = "toolsMenuItem";
      this.toolsMenuItem.Size = new System.Drawing.Size(44, 20);
      this.toolsMenuItem.Text = ResourceManager.Resource.GetString("toolsMenuItem");
      // 
      // proxyMenuItem
      // 
      this.proxyMenuItem.CheckOnClick = true;
      this.proxyMenuItem.Name = "proxyMenuItem";
      this.proxyMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.P)));
      this.proxyMenuItem.Size = new System.Drawing.Size(152, 22);
      this.proxyMenuItem.Text = ResourceManager.Resource.GetString("proxyMenuItem");
      this.proxyMenuItem.CheckedChanged += new System.EventHandler(this.proxyMenuItem_CheckedChanged);
      // 
      // Separator_1
      // 
      this.fSeparator.Name = "firstSeparator";
      this.fSeparator.Size = new System.Drawing.Size(156, 6);
      // 
      // Separator_2
      // 
      this.sSeparator.Name = "secondSeparator";
      this.sSeparator.Size = new System.Drawing.Size(156, 6);
      // 
      // messageLabel
      // 
      this.messageLabel.Location = (Equals(ResourceManager.Resource.GetString("RightToLeft").ToLower(), Constants.SLUG.DEFAULTSLUGS[8])) ? new System.Drawing.Point(210, 185) : new System.Drawing.Point(213, 185);
      this.messageLabel.Name = "messageLabel";
      this.messageLabel.AutoSize = true;
      this.messageLabel.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      // 
      // mainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(658, 355);
      this.Controls.Add(this.messageLabel);
      this.Controls.Add(this.cogOffered);
      this.Controls.Add(this.progressBar);
      this.Controls.Add(this.item9Button);
      this.Controls.Add(this.item8Button);
      this.Controls.Add(this.item7Button);
      this.Controls.Add(this.item6Button);
      this.Controls.Add(this.item5Button);
      this.Controls.Add(this.item4Button);
      this.Controls.Add(this.item3Button);
      this.Controls.Add(this.item2Button);
      this.Controls.Add(this.item1Button);
      this.Controls.Add(this.taskpanelBox);
      this.Controls.Add(this.logininfoBox);
      this.Controls.Add(this.headerLogo);
      this.Controls.Add(this.mainMenu);
      this.MainMenuStrip = this.mainMenu;
      this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.Name = "mainForm";
      this.RightToLeftLayout = true;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("RightToLeft"));
      this.Text = ResourceManager.Resource.GetString("ApplicationName");
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.mainForm_FormClosed);
      ((System.ComponentModel.ISupportInitialize)(this.headerLogo)).EndInit();
      this.logininfoBox.ResumeLayout(false);
      this.logininfoBox.PerformLayout();
      this.taskpanelBox.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.mpoNumDown)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nopNumDown)).EndInit();
      this.mainMenu.ResumeLayout(false);
      this.mainMenu.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();
    }
    #endregion

    private System.Windows.Forms.PictureBox headerLogo;
    private System.Windows.Forms.GroupBox logininfoBox;
    private System.Windows.Forms.Label usernameLabel;
    private System.Windows.Forms.Label serverLabel;
    private System.Windows.Forms.Label silverLabel;
    private System.Windows.Forms.Label usernameValue;
    private System.Windows.Forms.Label serverValue;
    private System.Windows.Forms.Label silverValue;
    private System.Object[] KEYS;
    private System.Windows.Forms.GroupBox taskpanelBox;
    private System.Windows.Forms.RadioButton item1Button;
    private System.Windows.Forms.RadioButton item2Button;
    private System.Windows.Forms.RadioButton item3Button;
    private System.Windows.Forms.RadioButton item4Button;
    private System.Windows.Forms.RadioButton item5Button;
    private System.Windows.Forms.RadioButton item6Button;
    private System.Windows.Forms.RadioButton item7Button;
    private System.Windows.Forms.RadioButton item8Button;
    private System.Windows.Forms.RadioButton item9Button;
    private System.Windows.Forms.ProgressBar progressBar;
    private System.Windows.Forms.Label nopLabel;
    private System.Windows.Forms.NumericUpDown nopNumDown;
    private System.Windows.Forms.Label mpoLabel;
    private System.Windows.Forms.NumericUpDown mpoNumDown;
    private System.Windows.Forms.Label cogLabel;
    private System.Windows.Forms.Label cogOffered;
    private System.Windows.Forms.Button stopButton;
    private System.Windows.Forms.Button startButton;
    private System.Windows.Forms.Label messageLabel;
    private System.Windows.Forms.MenuStrip mainMenu;
    private System.Windows.Forms.ToolStripMenuItem fileMenuItem;
    private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem helpMenuItem;
    private System.Windows.Forms.ToolStripMenuItem aboutMenuItem;
    private System.Windows.Forms.ToolStripMenuItem sourceMenuItem;
    private System.Windows.Forms.ToolStripMenuItem supportMenuItem;
    private System.Windows.Forms.ToolStripMenuItem homepageMenuItem;
    private System.Windows.Forms.ToolStripMenuItem toolsMenuItem;
    private System.Windows.Forms.ToolStripMenuItem proxyMenuItem;
    private System.Windows.Forms.ToolStripSeparator fSeparator;
    private System.Windows.Forms.ToolStripSeparator sSeparator;
  }
}